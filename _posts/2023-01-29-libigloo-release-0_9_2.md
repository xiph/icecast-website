---
layout: post
title:  "libigloo Release 0.9.2"
date:   2023-01-29
author: phschafft
categories: libigloo release
---

We are pleased to announce a new release of libigloo!

The changes include:

* Improved performance of the string pool
* Fixed reference counting in ro
* Moved to librhash as backend for cryptographic hash calculation

The fixed bug in the reference counting could result in invalid memory usage patterns.
Such invalid patterns could result in security and/or stability issues.
Therefore we strongly recommend to upgrade.

Download libigloo 0.9.2 from the [download page]({{ "/download" | prepend: site.baseurl }}).
